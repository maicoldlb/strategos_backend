# strategos Backend

## Build Setup


```bash
# clone the project
git clone https://gitlab.com/maicoldlb/strategos_backend.git

# enter the project directory
cd strategos_backend

# create .env  python enviroment
mkdir .env
cd .env
python3 -m venv .

# Activate python env

source bin/activate

cd ..

pip install requirements.txt 

## License

[MIT](https://gitlab.com/luisparedesa/strategos/master/LICENSE) license.

Copyright (c) 2020-present Strategos
