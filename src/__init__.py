""" Logging """

import logging



TEMPLATE = '%(asctime)s:%(name)s:%(message)s'
formatter = logging.Formatter(TEMPLATE)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
