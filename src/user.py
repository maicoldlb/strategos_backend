from flask import Flask, jsonify, make_response, request, Response
from flask_restful import reqparse, Resource, abort
from uuid import uuid4

from .odoo import Odoo

import logging
import json


logger = logging.getLogger(__name__)


class Login(Resource):
    def __init__(self):
        self.odoo = Odoo()
        self.model_name = 'res.users'
    
    def post(self):
        try:
            vals = request.get_json(force=True)
            username = vals.get('login')
            password = vals.get('password')
            user = self.odoo.login(username, password)
            if user:
                rand_token = uuid4()
                data = {
                    'access_token': rand_token.hex,
                    'image': user.partner_id['image_1920'],
                    'user': {
                        'id': user.partner_id['id'],
                        'name': user.partner_id['name']
                    }
                }
                return make_response(jsonify(data), 200)
            else:
                return make_response(jsonify({'message': 'Usuario y/o contraseña incorrecto'}), 401)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify({'message': 'Error'}), 500)


class Logout(Resource):
    def __init__(self):
        self.odoo = Odoo()
        self.model_name = 'res.users'
    
    def post(self):
        try:
            vals = request.get_json(force=True)
            access_token = vals.get('access_token')
            return make_response(jsonify({'sucess': True}), 200)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify({'message': 'Error'}), 500)