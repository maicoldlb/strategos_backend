from odoo_rpc_client import Client
from configparser import ConfigParser
import logging

CONFIG = ConfigParser()
CONFIG.read('config.ini')


logger = logging.getLogger(__name__)



class Odoo:
    def __init__(self):
        self.client = Client(
            host=CONFIG['HOST']['HOSTNAME'], 
            dbname=CONFIG['DATABASE']['DB'], 
            user=CONFIG['DATABASE']['USERNAME'],
            pwd=CONFIG['DATABASE']['PASSWORD'],
            port=CONFIG['HOST']['PORT']
        )
    
    def get(self, model_name, ids=None, domain=None):
        model = self.client[model_name]
        if ids is None:
            ids = model.search([])
        if isinstance(ids, str) and ids.isdigit():
            ids = int(ids)
        if isinstance(domain, list):
            ids = model.search(domain)
        return model.read(ids)
    
    def post(self, model_name, vals):
        return self.client[model_name].create(vals)
    
    def put(self, model_name, ids, vals):
        return self.client[model_name].write(ids, vals)
    
    def delete(self, model_name, ids):
        return self.client[model_name].unlink(ids)
    
    
    def login(self, username, password):
        try:
            client = self.client.login(CONFIG['DATABASE']['DB'], username, password)
            if client.user:
                return client.user
        except Exception as e:
            logger.exception(e)
            return False
