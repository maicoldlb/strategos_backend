from .user import Login, Logout
from .program import Program
from .course import Course, Courses

def initialize_routes(api):
    api.add_resource(Login, '/user/login')
    api.add_resource(Logout, '/user/logout')
    api.add_resource(Program, '/program')
    api.add_resource(Course, '/course')
    api.add_resource(Courses, '/courses')