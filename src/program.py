from flask import Flask, jsonify, make_response, request
from flask_restful import Resource
from .odoo import Odoo

import logging
import json


logger = logging.getLogger(__name__)


class Program(Resource):
    def __init__(self):
        self.odoo = Odoo()
        self.model_name = 'lms.course.inscription.line'
    
    def get(self):
        try:
            record_id = request.args.get('id')
            if record_id:
                record_id = int(record_id)
            domain = [('students_id', '=', record_id)]
            records = self.odoo.get(self.model_name, domain=domain)
            return make_response(jsonify(records), 200)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify(), 500)

    def post(self):
        try:
            vals = request.get_json(force=True)
            record_id = self.odoo.post(self.model_name, vals)
            return make_response(jsonify({'id': record_id}), 201)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify({'message': 'Error'}), 500)
    
    def put(self):
        try:
            vals = request.get_json(force=True)
            record_id = request.args.get('id')
            response = self.odoo.put(self.model_name, record_id, vals)
            return make_response(jsonify({'response': response}), 200)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify({'message': 'Error'}), 500)
    
    def delete(self):
        try:
            record_id = request.args.get('id')
            response = self.odoo.delete(self.model_name, record_id)
            return make_response(jsonify({'response': response}), 200)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify({'message': 'Error'}), 500)