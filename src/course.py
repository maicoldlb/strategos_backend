from flask import Flask, jsonify, make_response, request
from flask_restful import Resource
from .odoo import Odoo

import logging
import json


logger = logging.getLogger(__name__)


class Course(Resource):
    def __init__(self):
        self.odoo = Odoo()
        self.model_name = 'lms.course'
        self.model_inscription_line = 'lms.course.inscription.line'
    
    def get(self):
        try:
            record_id = request.args.get('id')
            records = self.odoo.get(self.model_name, record_id)
            media_ids = self.odoo.get('lms.course.line.media', records.get('media_ids'))
            objetive_ids = self.odoo.get('lms.course.line.objetive', records.get('objetive_ids'))
            material_ids = self.odoo.get('lms.course.line.materials', records.get('material_ids'))
            assignment_ids = self.odoo.get('lms.course.assignment', records.get('assignment_ids'))
            records.update({
                'media_ids': media_ids,
                'objetive_ids': objetive_ids,
                'material_ids': material_ids,
                'assignment_ids': assignment_ids
            })
            return make_response(jsonify(records), 200)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify(), 500)

    def post(self):
        try:
            vals = request.get_json(force=True)
            record_id = self.odoo.post(self.model_name, vals)
            return make_response(jsonify({'id': record_id}), 201)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify({'message': 'Error'}), 500)
    
    def put(self):
        try:
            vals = request.get_json(force=True)
            record_id = request.args.get('id')
            response = self.odoo.put(self.model_name, record_id, vals)
            return make_response(jsonify({'response': response}), 200)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify({'message': 'Error'}), 500)
    
    def delete(self):
        try:
            record_id = request.args.get('id')
            response = self.odoo.delete(self.model_name, record_id)
            return make_response(jsonify({'response': response}), 200)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify({'message': 'Error'}), 500)


class Courses(Resource):
    def __init__(self):
        self.odoo = Odoo()
        self.model_name = 'lms.course'
        self.model_inscription_line = 'lms.course.inscription.line'
    
    def get(self):
        try:
            record_id = request.args.get('id')
            program_id = request.args.get('program_id')
            domain = [
                ('students_id', '=', int(record_id)),
                ('program_id', '=', int(program_id))
            ]
            inscriptions = self.odoo.get(self.model_inscription_line, domain=domain)
            return make_response(jsonify(inscriptions), 200)
        except Exception as e:
            logger.exception(e)
            return make_response(jsonify(), 500)