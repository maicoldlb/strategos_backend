""" Main """
from flask import Flask
from flask_cors import CORS
from flask_restful import Api

from src.routers import initialize_routes


app = Flask(__name__)
CORS(app, resources={r"*": {"origins": "*"}})
api = Api(app)
initialize_routes(api)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8090, debug=True)
